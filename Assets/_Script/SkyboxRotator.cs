﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class SkyboxRotator : MonoBehaviour
{
	public float speed = 1;
	public Transform sun;
	private float angle;

	private void Start()
	{
		var randomAngle = Random.Range(0, 360);

		angle += randomAngle;

		if (sun)
			sun.eulerAngles += Vector3.up * randomAngle * -1;
	}

	void Update()
	{
		if (angle >= 360 && speed > 0)
		{
			angle = 0;
		}
		if (angle <= 0 && speed < 0)
		{
			angle = 360;
		}

		angle += Time.deltaTime * speed;

		if (sun)
			sun.eulerAngles += Vector3.up * Time.deltaTime * speed * -1;

		RenderSettings.skybox.SetFloat("_Rotation", angle);
	}
}
