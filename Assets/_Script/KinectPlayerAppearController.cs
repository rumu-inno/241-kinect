﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KinectPlayerAppearController : MonoBehaviour
{
	public struct OnDetect
	{
		public bool isDetected;
		public SSFSController vfxController;
	}

	private OnDetect[] onDetects;
	private KinectManager manager
	{
		get { return KinectManager.Instance; }
	}

	private void Start()
	{
		var controllers = GetComponentsInChildren<SSFSController>();

		onDetects = new OnDetect[controllers.Length];

		for (int i = 0; i < onDetects.Length; i++)
			onDetects[i].vfxController = controllers[i];
	}

	void Update()
	{
		for (int i = 0; i < onDetects.Length; i++)
		{
			var user = onDetects[i];

			if (!user.isDetected && manager.IsUserDetected(i))
			{
				user.isDetected = true;
				user.vfxController.Appear();
			}
			else if (user.isDetected && !manager.IsUserDetected(i))
			{
				user.vfxController.Disappear();
				user.isDetected = false;
			}

		}
	}
}
