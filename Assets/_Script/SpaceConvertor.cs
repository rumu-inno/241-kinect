﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceConvertor : MonoBehaviour
{
	private LineRenderer line;

	private void Update()
	{
		line = GetComponentInChildren<LineRenderer>();
		GetComponent<Handwriting>().OnTimesUp += CloneTo;
	}

	public void CloneTo()
	{
		line.transform.localPosition = line.transform.parent.position * -1;
		line.useWorldSpace = false;
	}
}
