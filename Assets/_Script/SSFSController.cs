﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSFSController : MonoBehaviour
{
	public void Appear()
	{
		float phase = GetComponent<Renderer>().material.GetFloat("_Phase");
		DOTween.To(() => phase, p => phase = p, 1, 8)
			.OnUpdate(() => GetComponent<Renderer>().material.SetFloat("_Phase", phase))
			.OnStart(() => gameObject.SetActive(true));
	}

	public void Disappear()
	{
		float phase = GetComponent<Renderer>().material.GetFloat("_Phase");
		DOTween.To(() => phase, p => phase = p, 0, 8)
			.OnUpdate(() => GetComponent<Renderer>().material.SetFloat("_Phase", phase))
			.OnComplete(() => gameObject.SetActive(false));
	}
}
