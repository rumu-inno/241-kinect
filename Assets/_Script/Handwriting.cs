﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

public class Handwriting : MonoBehaviour
{
	private LineRenderer trail;
	private VelocityEstimator estimator;

	public Transform target;
	public float smoothTime;
	public float smoothMaxSpeed;
	public float timeLimit = Mathf.Infinity;

	private Vector3 refVelocity;
	private float leftTime;
	private bool isWriting;

	public event Action OnTimesUp = delegate { };
	[SerializeField]
	private UnityEvent onTimesUp;

	private void Start()
	{
		trail = GetComponent<LineRenderer>();
		estimator = GetComponent<VelocityEstimator>();

		leftTime = 0;
	}

	void Update()
	{
		if (leftTime < timeLimit)
		{
			leftTime += Time.deltaTime;
			isWriting = true;

			if (target != null)
				transform.position = Vector3.SmoothDamp(transform.position, target.position, ref refVelocity, smoothTime, smoothMaxSpeed);
		}
		else if (isWriting)
		{
			isWriting = false;
			OnTimesUp();
			onTimesUp.Invoke();
		}
	}
}
