﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Region : MonoBehaviour
{
    public Color color;
    public Vector3 size;

    public Vector3 GetPoint()
    {
        float minX = transform.position.x - (size.x / 2);
        float maxX = transform.position.x + (size.x / 2);

        float minY = transform.position.y - (size.y / 2);
        float maxY = transform.position.y + (size.y / 2);

        float minZ = transform.position.z - (size.z / 2);
        float maxZ = transform.position.z + (size.z / 2);

        float x = Random.Range(minX, maxX);
        float y = Random.Range(minY, maxY);
        float z = Random.Range(minZ, maxZ);

        return new Vector3(x, y, z);
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawCube(transform.position, size);
    }
}
