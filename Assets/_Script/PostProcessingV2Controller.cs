﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[ExecuteInEditMode]
public class PostProcessingV2Controller : MonoBehaviour
{
	public PostProcessProfile processResources;
	public float bloom;

	private void Update()
	{
		if (processResources != null)
			processResources.GetSetting<Bloom>().intensity.value = bloom;
	}
}
