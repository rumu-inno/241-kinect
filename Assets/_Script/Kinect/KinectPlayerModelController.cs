﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KinectPlayerModelController : MonoBehaviour
{
	private KinectManager manager
	{
		get { return KinectManager.Instance; }
	}
	public GameObject model1;
	public GameObject model2;
	public GameObject model3;
	public GameObject model4;
	public GameObject model5;
	public GameObject model6;

    public ParticleSystem flowerSurface1;
    public ParticleSystem flowerSurface2;
    public ParticleSystem flowerSurface3;
    public ParticleSystem flowerSurface4;
    public ParticleSystem flowerSurface5;
    public ParticleSystem flowerSurface6;

    bool findFlowerSurface1 = false;
    bool findFlowerSurface2 = false;
    bool findFlowerSurface3 = false;
    bool findFlowerSurface4 = false;
    bool findFlowerSurface5 = false;
    bool findFlowerSurface6 = false;

    private void Update()
	{
   
        if (model1)
        {
            if (manager.IsUserDetected(0))
            {
                model1.SetActive(true);

                if (!findFlowerSurface1)
                {               
                    findFlowerSurface1 = true;
                    flowerSurface1.startColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), .8f);                  
                }
            }
            else
            {
                model1.SetActive(false);
                findFlowerSurface1 = false;
            }
        }

        if (model2)
        {
            if (manager.IsUserDetected(1))
            {
                model2.SetActive(true);

                if (!findFlowerSurface2)
                {
                    findFlowerSurface2 = true;
                    flowerSurface2.startColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), .8f);
                }
            }
            else
            {
                model2.SetActive(false);
                findFlowerSurface2 = false;          
            }
        }

        if (model3)
        {
            if (manager.IsUserDetected(2))
            {
                model3.SetActive(true);

                if (!findFlowerSurface3)
                {
                    findFlowerSurface3 = true;
                    flowerSurface3.startColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), .8f);
                }
            }
            else
            {
                model3.SetActive(false);
                findFlowerSurface3 = false;          
            }
        }

        if (model4)
        {
            if (manager.IsUserDetected(3))
            {
                model4.SetActive(true);

                if (!findFlowerSurface4)
                {
                    findFlowerSurface4 = true;
                    flowerSurface4.startColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), .8f);
                }
            }
            else
            {
                model4.SetActive(false);
                findFlowerSurface4 = false;
            }
        }

        if (model5)
        {
            if (manager.IsUserDetected(4))
            {
                model5.SetActive(true);

                if (!findFlowerSurface5)
                {
                    findFlowerSurface5 = true;
                    flowerSurface5.startColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), .8f);
                }
            }
            else
            {
                model5.SetActive(false);
                findFlowerSurface5 = false;       
            }
        }

        if (model6)
        {
            if (manager.IsUserDetected(5))
            {
                model6.SetActive(true);

                if (!findFlowerSurface6)
                {
                    findFlowerSurface6 = true;
                    flowerSurface6.startColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), .8f);
                }
            }
            else
            {
                model6.SetActive(false);
                findFlowerSurface6 = false;

            }
        }
	}
}
