﻿using UnityEngine;

// 這個腳本功用是快速設定所有物件上面PlaneJointOverlayer的playerIndex，因為一個物件上面有太多要設定了
public class PlaneJointHelper : MonoBehaviour
{
	public int index;

	private void OnValidate()
	{
		foreach (var item in GetComponentsInChildren<PlaneJointOverlayer>())
		{
			item.playerIndex = index;
		}
	}
}
