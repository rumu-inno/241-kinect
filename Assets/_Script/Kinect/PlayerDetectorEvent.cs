﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerDetectorEvent : MonoBehaviour
{
    [System.Serializable]
    public struct DetectorEvent
    {
        public int playerIndex;
        public UnityEvent onDetected;
        public UnityEvent onLost;
    }

    private KinectManager manager { get { return KinectManager.Instance; } }
    public DetectorEvent[] detectorEvents;

    private void Update()
    {
        for (int i = 0; i < detectorEvents.Length; i++)
        {
            if (manager.IsUserDetected(detectorEvents[i].playerIndex))
                detectorEvents[i].onDetected.Invoke();
            else
                detectorEvents[i].onLost.Invoke();
        }
    }
}
