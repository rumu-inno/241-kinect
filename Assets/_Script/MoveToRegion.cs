﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToRegion : MonoBehaviour
{
	public Vector2 moveDuration = new Vector2(3.0f, 5.0f);
	public Ease easeType = Ease.InOutQuad;

	void Start()
	{
		var region = FindObjectOfType<Region>();
		transform.DOMove(region.GetPoint(), Random.Range(moveDuration.x, moveDuration.y))
				.SetEase(easeType)
				.OnComplete(() => Destroy(gameObject));
	}
}
