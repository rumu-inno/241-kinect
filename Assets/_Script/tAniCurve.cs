﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tAniCurve : MonoBehaviour
{
	public AnimationCurve curve;

	public void LateUpdate()
	{
		if (Input.GetKeyDown(KeyCode.T))
		{
			Add(curve, transform.position.y);
		}
	}

	private void Add(AnimationCurve refCurve, float value)
	{
		var keyframe = new Keyframe[refCurve.keys.Length + 1];
		keyframe[keyframe.Length - 1].time = 1;
		keyframe[keyframe.Length - 1].value = value;

		for (int i = 0; i < keyframe.Length - 1; i++)
			keyframe[i] = new Keyframe(1.0f / (keyframe.Length - 1) * i, refCurve[i].value);

		refCurve.keys = keyframe;
	}
}
