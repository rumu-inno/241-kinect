﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
	public string sceneToLoad;
	private bool active;

	public void LoadSceneAsync(Action<float> onLoading)
	{
		int sceneIndex;
		AsyncOperation async;

		if (int.TryParse(sceneToLoad, out sceneIndex))
		{
			async = SceneManager.LoadSceneAsync(sceneIndex);
		}
		else
		{
			async = SceneManager.LoadSceneAsync(sceneToLoad);
		}

		async.allowSceneActivation = false;

		StartCoroutine(LoadSceneAsyncCo(async, onLoading));
	}

	public void LoadSceneAsync()
	{
		int sceneIndex;
		AsyncOperation async;

		if (int.TryParse(sceneToLoad, out sceneIndex))
		{
			async = SceneManager.LoadSceneAsync(sceneIndex);
		}
		else
		{
			async = SceneManager.LoadSceneAsync(sceneToLoad);
		}

		async.allowSceneActivation = false;

		StartCoroutine(LoadSceneAsyncCo(async, null));
	}

	IEnumerator LoadSceneAsyncCo(AsyncOperation async, Action<float> onLoading)
	{
		while (async.progress < 0.9f)
		{
			if (onLoading != null)
				onLoading(async.progress);
			yield return null;
		}

		while (!active)
			yield return null;

		async.allowSceneActivation = true;
	}

	public void ReadyToActive()
	{
		active = true;
	}
}
