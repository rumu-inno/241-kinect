﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndexManager : MonoBehaviour
{
	public static IndexManager Instance { get; private set; }

	public Dictionary<ulong, int> playerIndex = new Dictionary<ulong, int>();



	private void Awake()
	{
		Instance = this;
	}

	public int? AddIndex(ulong id)
	{
		if (playerIndex.ContainsKey(id))
			return null;

		for (int index = 0; index < 6; index++)
		{
			if (!playerIndex.ContainsValue(index))
			{
				playerIndex.Add(id, index);
				return index;
			}
		}
		return null;
	}

	public void RemoveIndex(ulong id)
	{
		if (playerIndex.ContainsKey(id))
			playerIndex.Remove(id);
	}

	public bool HasInTracked(ulong id)
	{
		if (playerIndex.ContainsKey(id))
			return true;
		else
			return false;
	}

	public bool[] debug;
	private void Update()
	{
		for (int i = 0; i < debug.Length; i++)
		{
			if (playerIndex.ContainsValue(i))
			{
				debug[i] = true;
			}
			else
			{
				debug[i] = false;
			}
		}
	}
}
