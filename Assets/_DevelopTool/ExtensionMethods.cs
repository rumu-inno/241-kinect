﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.Events;

public enum Axis { X, Y, Z }

public static class ExtensionMethods
{
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    public static bool InRange<T>(this T value, T from, T to) where T : IComparable<T>
    {
        return value.CompareTo(from) >= 1 && value.CompareTo(to) <= -1;
    }

    public static Vector3 GetOneAxis(this Vector3 vector3, Axis axis)
    {
        Vector3 newVector = Vector3.zero;
        switch (axis)
        {
            case Axis.X:
                newVector = new Vector3(vector3.x, 0, 0);
                break;
            case Axis.Y:
                newVector = new Vector3(0, vector3.y, 0);
                break;
            case Axis.Z:
                newVector = new Vector3(0, 0, vector3.z);
                break;
        }
        return newVector;
    }

    public static List<T> AsQueue<T>(this List<T> list, int size, T type)
    {
        if (list.Count < size)
        {
            list.Add(type);
        }
        else
        {
            list.Remove(list[0]);
            list.Add(type);
        }
        return list;
    }

    public static string MethodsName(this UnityEvent uEvent, bool full = false)
    {
        string eventContent = "";

        if (uEvent.GetPersistentEventCount() == 0)
        {
            eventContent = "There are no method in this UnityEvent.";
        }
        else
        {
            for (int i = 0; i < uEvent.GetPersistentEventCount(); i++)
            {
                if (i > 0)
                {
                    eventContent += "\n";
                }

                if (full)
                    eventContent += uEvent.GetPersistentTarget(i) + ": " + uEvent.GetPersistentMethodName(i);
                else
                    eventContent += uEvent.GetPersistentMethodName(i);

            }
        }

        return eventContent;
    }

    //public static void AddTarget<T>(T target) where T : MonoBehaviour
    //{
    //    if (!targetList.Contains(target))
    //        targetList.Add(target);
    //}
}