﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BasicEvent : MonoBehaviour
{
    public UnityEvent start;
    public UnityEvent awake;
    public UnityEvent onEnable;
    public UnityEvent onDisable;
    public UnityEvent update;
    public UnityEvent onDestory;

    void Start()
    {
        start.Invoke();
    }

    void Update()
    {
        update.Invoke();
    }

    private void OnEnable()
    {
        onEnable.Invoke();
    }

    private void OnDisable()
    {
        onDisable.Invoke();
    }

    private void OnDestroy()
    {
        onDestory.Invoke();
    }

    private void Awake()
    {
        awake.Invoke();
    }
}
