﻿using UnityEngine;

[System.Serializable]
public struct RandomValue
{
    public float min;
    public float max;
    public float value
    {
        get { return Random.Range(min, max); }
    }
    public float select
    {
        get
        {
            if (Random.Range(0.0f, 1.0f) < 0.5f)
            {
                return min;
            }
            else
            {
                return max;
            }
        }
    }
}