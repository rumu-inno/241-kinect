﻿using UnityEditor;
using UnityEngine;

static class EditorMenus
{
    // taken from: http://answers.unity3d.com/questions/282959/set-inspector-lock-by-code.html
    /*

    # = Shift
    % = Ctrl
    & = Alt

     */
    [MenuItem("Tools/Toggle Inspector Lock &W")] // Alt + W
    static void ToggleInspectorLock()
    {
        ActiveEditorTracker.sharedTracker.isLocked = !ActiveEditorTracker.sharedTracker.isLocked;
        ActiveEditorTracker.sharedTracker.ForceRebuild();
    }


    [MenuItem("Tools/Reset Transform &Q")] // Alt + Q
    static void ResetTransform()
    {
        var transforms = Selection.transforms;

        foreach (var transform in transforms)
        {
            Transform[] children = new Transform[transform.childCount];

            for (int j = 0; j < transform.childCount; j++)
            {
                children[j] = transform.GetChild(j);
            }

            transform.DetachChildren();
            transform.position = Vector3.zero;
            transform.eulerAngles = Vector3.zero;
            transform.localScale = Vector3.one;

            for (int j = 0; j < children.Length; j++)
            {
                children[j].parent = transform;
            }
        }
    }

    [MenuItem("GameObject/Group Selected %g")]
    private static void GroupSelected()
    {
        if (!Selection.activeTransform) return;
        var go = new GameObject(Selection.activeTransform.name + " Group");
        Undo.RegisterCreatedObjectUndo(go, "Group Selected");
        go.transform.SetParent(Selection.activeTransform.parent, false);
        foreach (var transform in Selection.transforms) Undo.SetTransformParent(transform, go.transform, "Group Selected");
        Selection.activeGameObject = go;
    }

    [MenuItem("GameObject/Look At Camera %L")]
    static void LookAt()
    {
        var transforms = Selection.transforms;
        var go = transforms[0];
        Undo.RegisterCompleteObjectUndo(go, "Look At");
        go.LookAt(Camera.main.transform.position);
    }

    [MenuItem("GameObject/Invers Look At Camera %#L")]
    static void InversLookAt()
    {
        var transforms = Selection.transforms;
        var go = transforms[0];
        Undo.RegisterCompleteObjectUndo(go, "Invers Look At");
        go.LookAt(go.position + (go.position - Camera.main.transform.position).normalized);
    }
}
