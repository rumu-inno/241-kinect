﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[InitializeOnLoad]
class MyHierarchyIcon
{
    static Texture2D texture;
    static List<int> markedObjects;

    static MyHierarchyIcon()
    {
        // Init
        texture = AssetDatabase.LoadAssetAtPath("Assets/_DevelopTool/Editor/Img/folder.png", typeof(Texture2D)) as Texture2D;
        EditorApplication.update += UpdateCB;
        EditorApplication.hierarchyWindowItemOnGUI += HierarchyItemCB;
    }

    static void UpdateCB()
    {
        // Check here
        GameObject[] go = Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];

        markedObjects = new List<int>();
        foreach (GameObject g in go)
        {
            // Example: mark all lights
            Component[] components = g.GetComponents<Component>();
            bool hasChild = g.transform.childCount > 0;
            if (hasChild && components.Length == 1/*GetComponent<Transform>() != null*/)
                markedObjects.Add(g.GetInstanceID());
        }

    }

    static void HierarchyItemCB(int instanceID, Rect selectionRect)
    {
        // place the icoon to the right of the list:
        Rect r = new Rect(selectionRect)
        {
            x = 0,
            width = 18
        };

        if (!EditorApplication.isPlaying && markedObjects != null)
        {
            // Draw the texture if it's a light (e.g.)
            if (markedObjects.Contains(instanceID))
                GUI.Label(r, texture);
        }
    }

}

