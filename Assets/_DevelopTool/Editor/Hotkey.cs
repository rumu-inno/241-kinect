﻿// using UnityEngine;
using UnityEditor;

public class Hotkey : EditorWindow
{
    [MenuItem("Hotkey/Align View to Selected &#f")] // Alt+Shift+F
    public static void HotkeyAlignViewToSelected()
    {
        if (SceneView.lastActiveSceneView != null && Selection.activeTransform != null)
        {
            SceneView.lastActiveSceneView.AlignViewToObject(Selection.activeTransform);
        }
    }
}

// Reference:
// https://github.com/MattRix/UnityDecompiled/blob/master/UnityEditor/UnityEditor/SceneView.cs