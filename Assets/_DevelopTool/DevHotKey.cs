﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public class DevHotKey : MonoBehaviour
{
	[Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	[Serializable]
	public class KeyMap
	{
		public string description;
		public KeyCode key;
		public bool onlyUsedInEditor;
		public UnityEvent function;
	}

	[Header("Reload Scene")]
	public KeyCode reloadScene = KeyCode.F5;
	public bool onlyUsedInEditor;

	[Header("Show KeyMap Function OnGUI")]
	public KeyCode showInstruction = KeyCode.None;
	public bool invokeFalseOnStart;
	public BoolEvent onToggleInstruction;
	private bool show;

	[Header("Fucntion KeyMap")]
	public KeyMap[] keyMaps;

	private void OnValidate()
	{
		if (keyMaps.Length > 0)
			foreach (var keymap in keyMaps)
				keymap.description = keymap.description == "" ? keymap.function.MethodsName() : keymap.description;
	}

	private void Start()
	{
		onToggleInstruction.Invoke(!invokeFalseOnStart);
	}

	private void Update()
	{
		bool work = onlyUsedInEditor ? Application.isEditor : true;

		if (work && Input.GetKeyDown(reloadScene))
		{
			Restart();
		}

		if (Input.GetKeyDown(showInstruction))
		{
			show = !show;
			onToggleInstruction.Invoke(show);
		}

		foreach (var keyMap in keyMaps)
		{
			bool workKeyMap = keyMap.onlyUsedInEditor ? Application.isEditor : true;

			if (workKeyMap && Input.GetKeyDown(keyMap.key))
				keyMap.function.Invoke();
		}
	}

	private void OnGUI()
	{
		if (!show) return;

		GUILayout.TextField(string.Format("Press「{0}」to 「Hide function key description」", showInstruction));

		if (!onlyUsedInEditor)
		{
			GUILayout.TextField(string.Format("Press「{0}」to 「Reload current scene」", reloadScene));
		}
		else
		{
			if (Application.isEditor)
				GUILayout.TextField(string.Format("Press「{0}」to 「Reload current scene」", reloadScene));
		}

		foreach (var keyMap in keyMaps)
		{
			if (!keyMap.onlyUsedInEditor)
			{
				GUILayout.TextField(string.Format("Press「{0}」to「{1}」", keyMap.key, keyMap.description));
			}
			else
			{
				if (Application.isEditor)
					GUILayout.TextField(string.Format("Press「{0}」to「{1}」", keyMap.key, keyMap.description));
			}
		}
	}

	public void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void ChangeScene(string name)
	{
		SceneManager.LoadScene(name);
	}

	//     public void HotKey(KeyCode key, Action callback){
	// hotkeyDict.Add(key,callback);

	//     }

	//     public Dictionary<KeyCode,Action> hotkeyDict;

	// public Update(){
	//     foreach(var hotkey in hotkeyDict){
	//       if(GetKeyDown(hotkey.key)){
	//           hotkey.value();
	//       }
	//     }    

	//}


#if UNITY_EDITOR
	[ContextMenu("Set GameObejct's Name")]
	private void SetName()
	{
		gameObject.name = "DevHotKey";
	}
#endif
}
